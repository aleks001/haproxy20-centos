FROM centos:latest

# take a look at http://www.lua.org/download.html for
# newer version

# change for snapshot build
# + HAPROXY_VERSION=2.0.1
# + DEV= 
# +   && echo "${HAPROXY_SHA256} haproxy-${HAPROXY_VERSION}.tar.gz" | sha256sum -c \

ENV HAPROXY_MAJOR=2.0 \
    HAPROXY_VERSION=2.0.8 \
    DEV= \
    HAPROXY_SHA256=c37e1e8515ad6f9781a0ac336ca88787f3bb52252fb2bdad9919ba16323c280a \
    LUA_VERSION=5.3.5 \
    LUA_URL=http://www.lua.org/ftp/lua-5.3.5.tar.gz \
    LUA_SHA256=112eb10ff04d1b4c9898e121d6bdf54a81482447 \
    OPENSSL_VERS=1.1.1d \
    OSSP_SHA256=1e3a91bc1f9dfce01af26026f856e064eab4c8ee0a8f457b5ae30b40b8b711f2
    
# have some errors
#   && yum -y update 

RUN set -x \
  && yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
  && export buildDeps='diffutils which python3 git pcre-devel openssl-devel gcc make zlib-devel readline-devel openssl perl-Module-Load-Conditional perl-Test-Harness libslz-devel ' \
  && yum -y install pcre zlib libslz bind-utils curl iproute tar strace python3 ${buildDeps} \
  && mkdir -p /usr/src/openssl /usr/src/lua /usr/src/haproxy \
  && curl -sSLO https://www.openssl.org/source/openssl-${OPENSSL_VERS}.tar.gz \
  && echo "${OSSP_SHA256} openssl-${OPENSSL_VERS}.tar.gz" | sha256sum -c \
  && tar xfvz openssl-${OPENSSL_VERS}.tar.gz -C /usr/src/openssl --strip-components=1 \
  && cd /usr/src/openssl \
  && ./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl shared zlib \
  && make \
  && make install \
  && echo "pathmunge /usr/local/openssl/bin" > /etc/profile.d/openssl.sh \
  && echo "/usr/local/openssl/lib" > /etc/ld.so.conf.d/openssl-${OPENSSL_VERS}.conf \
  && ldconfig -v \
  && cd /usr/src/ \
  && git clone https://github.com/vtest/VTest.git \
  && cd VTest \
  && make vtest \
  && cd /usr/src/ \
  && curl -sSLO ${LUA_URL} \
  && echo "${LUA_SHA256} lua-${LUA_VERSION}.tar.gz" | sha1sum -c - \
  && tar -xzf lua-${LUA_VERSION}.tar.gz -C /usr/src/lua --strip-components=1 \
  && make -C /usr/src/lua linux test install \
  && curl -sSLO http://www.haproxy.org/download/${HAPROXY_MAJOR}/src/${DEV}/haproxy-${HAPROXY_VERSION}.tar.gz \
  && echo "${HAPROXY_SHA256} haproxy-${HAPROXY_VERSION}.tar.gz" | sha256sum -c \
  && tar -xzf haproxy-${HAPROXY_VERSION}.tar.gz  -C /usr/src/haproxy --strip-components=1 \
  && make -C /usr/src/haproxy  \
       TARGET=linux-glibc \
       USE_PCRE=1 \
       USE_OPENSSL=1 \
       SSL_INC=/usr/local/openssl/include \
       SSL_LIB=/usr/local/openssl/lib \
       USE_PCRE_JIT=1 \
       USE_LUA=1 \
       USE_PTHREAD_PSHARED=1 \
       USE_REGPARM=1 \
       USE_SLZ=1 \
       EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o" \
       all \
       install-bin \
  && mkdir -p /usr/local/etc/haproxy \
  && mkdir -p /usr/local/etc/haproxy/ssl \
  && mkdir -p /usr/local/etc/haproxy/ssl/cas \
  && mkdir -p /usr/local/etc/haproxy/ssl/crts \
  && cp -R /usr/src/haproxy/examples/errorfiles /usr/local/etc/haproxy/errors \
  && make -C /usr/src/haproxy/contrib/spoa_server/ install USE_LUA=1 \
  && cd /usr/src/haproxy \
  && VTEST_PROGRAM=/usr/src/VTest/vtest HAPROXY_PROGRAM=/usr/local/sbin/haproxy \
      make reg-tests \
  ; egrep -r ^ /tmp/haregtests*/* \
  ; yum -y autoremove $buildDeps \
  && yum -y clean all \
  && rm -rf /usr/src/ \
  && /usr/local/sbin/haproxy -vv

#         && openssl dhparam -out /usr/local/etc/haproxy/ssl/dh-param_4096 4096 \

COPY containerfiles /

RUN chmod 555 /container-entrypoint.sh

EXPOSE 13443

ENTRYPOINT ["/container-entrypoint.sh"]

#CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.conf"]
#CMD ["haproxy", "-vv"]
